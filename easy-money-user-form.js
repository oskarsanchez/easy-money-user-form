class EasyMoneyUserForm extends Polymer.Element {

  static get is() {
    return 'easy-money-user-form';
  }

  static get properties() {
    return {
        firstName: {
          type: String
        },
        lastName: {
          type: String
        },
        email: {
          type: String
        },
        password: {
          type: String
        },
        gender: {
          type: String
        },
        _isMan:{
          type: Boolean,
          computed: '_computeMan(gender)'
        },
        avatar: {
          type: String
        },
        userId: {
          type: Number
        },
        user:{
          type: Object
        },
        loading:{
          type: Boolean,
          notify: true,
          value: false
        },
        token:{
          type: String
        }

    };
  }
  _computeMan(gender){
    if (gender == "man")
      this.$.radioGroupGender.selected = 0;
    if (gender == "woman")
      this.$.radioGroupGender.selected = 1;

    if (gender == undefined)
      return null;
    return (gender == "man") ? true: false;
  }

  ready (){
    super.ready();
  }

  _handleCancelarClick(evt){
    this.dispatchEvent(new CustomEvent('edit-user-cancel',{bubbles:true, composed:true}));
  }

  _handleAceptarClick(evt){
    this.loading = true;
    const normalizedUser = {
      first_name: this.firstName,
      last_name : this.lastName,
      password: this.password,
      email: this.email,
      gender: this.gender,
      avatar: this.avatar
    };
    this.$.edituserdp.host = cells.urlEasyMoneyBankAuthServices;
    this.$.adduserdp.host = cells.urlEasyMoneyBankAuthServices;
    if (this.userId != undefined){
      this.$.edituserdp.headers = {
        authorization : "JWT " + sessionStorage.getItem("token"),
        user_id : parseInt(sessionStorage.getItem("userId"))
      };
      normalizedUser.id = this.userId;
      this.$.edituserdp.body = normalizedUser;
      this.$.edituserdp.generateRequest();
    }
    else {
      this.$.adduserdp.body = normalizedUser;
      this.$.adduserdp.generateRequest();
    }
  }

  _handleRequestEditUserSuccess(evt) {
    const detail = evt.detail;
    delete detail.headers;
    const normalizedUser = {
      id : detail.id,
      firstName: detail.firstName,
      lastName : detail.lastName,
      email: detail.email,
      gender: detail.gender,
      avatar: detail.avatar
    };
    this.user = normalizedUser;
    this.loading = false;
    this.dispatchEvent(new CustomEvent('edit-user-ok',{bubbles:true, composed:true}));
  }

  _handleRequestEditUserError(evt) {
    const detail = evt.detail;
    let msg;
    if (detail) {
      delete detail.headers;
      msg = detail.msg
    }
    this.loading = false;
    this.dispatchEvent(new CustomEvent('edit-user-error', {
      bubbles: true,
      composed: true,
      detail: {
        msg: msg
      }
    }));
  }

  _handleGenderGroupSelectedChange(evt){
    const detail = evt.detail;
    delete detail.headers;
    this.gender = (detail.value == 0)? "man" : "woman";
  }

  _handleRequestAddUserSuccess(evt) {
    const detail = evt.detail;
    delete detail.headers;
    const normalizedUser = {
      id : detail.id,
      firstName: detail.firstName,
      lastName : detail.lastName,
      email: detail.email,
      gender: detail.gender,
      avatar: detail.avatar
    };
    this.user = normalizedUser;
    this.token = detail.token;
    this.loading = false;
    this.dispatchEvent(new CustomEvent('add-user-ok',{bubbles:true, composed:true}));
  }

  _handleRequestAddUserError(evt) {
    const detail = evt.detail;
    let msg;
    if (detail) {
      delete detail.headers;
      msg = detail.msg
    }
    this.loading = false;
    this.dispatchEvent(new CustomEvent('add-user-error', {
      bubbles: true,
      composed: true,
      detail: {
        msg: msg
      }
    }));
  }
}
customElements.define(EasyMoneyUserForm.is, EasyMoneyUserForm);